#!/usr/bin/env atf-sh

. $(atf_get_srcdir)/test_env.sh
init_tests \
	setup_sshd_usage \
	setup_sshd_empty \
	setup_sshd_dropbear \
	setup_sshd_openssh \
	setup_sshd_interactive

setup_sshd_usage_body() {
	test_usage setup-sshd
}

setup_sshd_empty_body() {
	init_env
	atf_check -s exit:0 \
		-e empty \
		-o empty \
		setup-sshd none
}

setup_sshd_dropbear_body() {
	init_env
	atf_check -s exit:0 \
		-e empty \
		-o match:"^apk add .* dropbear" \
		-o match:"^rc-update add dropbear" \
		-o match:"^rc-service dropbear start" \
		setup-sshd dropbear
}

setup_sshd_openssh_body() {
	init_env
	atf_check -s exit:0 \
		-e empty \
		-o match:"^apk add .* openssh" \
		-o match:"^rc-update add sshd" \
		-o match:"^rc-service sshd start" \
		setup-sshd -k 'https://example.com/user.keys' openssh
	grep '^wget .*https://example.com/user.keys$' root/.ssh/authorized_keys || atf_fail "failed to wget ssh key"
}

setup_sshd_interactive_body() {
	init_env
	mkdir -p etc/ssh
	echo "PermitRootLogin foobar" > etc/ssh/sshd_config
	printf "%s\n%s\n" openssh no | atf_check -s exit:0 \
		-e empty \
		-o match:"Which SSH server" \
		-o match:"^apk add .* openssh" \
		-o match:"^rc-update add sshd" \
		-o match:"^rc-service sshd start" \
		setup-sshd
	grep '^PermitRootLogin no$' etc/ssh/sshd_config || atf_fail "did not set PermitRootLogin"
}

